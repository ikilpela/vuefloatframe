
// Draggable frame component. The title of the frame can be set by setting attribute ftitle in HTML.
// The content of the frame is automatically inserted in the correct div as the component only has a single slot. 
// The frame can be minimized and closed with buttons (NOTE: currently close just hides the panel)
var floatFrame = Vue.component('floatingframe', {
	template: `<section>
              <div class="section-header" :class="[dragged ? 'grabbed' : '']" @mousedown="onHeaderClick">
                <h2> {{ftitle}} </h2>
                <div class="section-utils">
                  <span v-on:click="toggle">
                    <i :class="[fminimized ? 'fas fa-plus fa-lg' : 'fas fa-minus fa-lg']"/>
                  </span>
                  <span v-on:click="close">
                    <i class="fas fa-times fa-lg"/>
                  </span>
                </div>
              </div>
              <div class="section-content" :class="[fminimized ? 'minimized' : '']">
                <slot>
                </slot>
              </div>
						</section>`,
	data() {
		return { title: "test", origLocation: {x:0,y:0}, origMouse: {x:0,y:0}, dragged: false}
	},
  props: [
    'ftitle','fminimized'
  ],
  methods: {
		// Header click event handling. Dragging mouse after clicking the header moves the entire frame.
    onHeaderClick: function(e) {
      e.preventDefault();
      document.body.style.cursor = "-webkit-grabbing !important"
      this.origLocation.x = this.$el.offsetLeft;
      this.origLocation.y = this.$el.offsetTop;
      this.origMouse.x = e.clientX;
      this.origMouse.y = e.clientY;
			// Add listener for mouse move (moves the frame) and mouseup (removes listeners)
      document.addEventListener('mousemove',this.onMouseMove)
      document.addEventListener('mouseup',this.onMouseUp)
    },
		// Mouse move handler which moves the frame along with cursor.
    onMouseMove: function (e) {
      var dx = e.clientX - this.origMouse.x;
      var dy = e.clientY - this.origMouse.y;
      this.$el.style.top = this.origLocation.y + dy + 'px';
      this.$el.style.left = this.origLocation.x + dx + 'px';
      // return false;
    },
		// Mouse up function that removes the listeners from the frame and sets dragged state to false
    onMouseUp: function(e) {
      document.removeEventListener('mousemove',this.onMouseMove)
      document.removeEventListener('mouseup',this.onMouseUp)
      var src = e.target || e.srcElement
      src.classList.remove("grabbed");
    },
		// Close button click handler
    close: function() {
      this.$el.style.display = "none";
    },
		// Minimize button handler
    toggle: function() {
      this.fminimized = !this.fminimized;
    }
  }
});

var desktopVue = new Vue({
  el:"#desktop",
  components: { floatFrame },
})
