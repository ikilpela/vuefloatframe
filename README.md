# Vue Floating Frames

Simple project made while getting first touches with Vue.js. 
 
Shows a couple of draggable frames on a page. The frames are Vue components for which the title can be set by setting attribute "ftitle" in HTML markup and elements inside the <floatingframe> tags are automatically included in the content section of the component.

